import Partner from "../models/Partner.js";
import AdminSubscription from "../models/AdminSubscription.js";
import PartnerSubscription from "../models/PartnerSubscription.js"
import dotenv from "dotenv"
import axios from "axios";
import cron from "node-cron"
dotenv.config();
const MOYASAR_API_KEY = process.env.MOYASAR_SECRET_KEY; // Replace with your actual API key
const MOYASAR_API_BASE_URL = 'https://api.moyasar.com/v1';
export const createPartnerSubscription = async (req, res, next) => {
    const newPartnerSubscription = new PartnerSubscription(req.body)
    let partner

    try {
        const savedPartnerSubscription = await newPartnerSubscription.save()
        try {

            const data = { amount: req.body.amount, description: req.body.description, success_url: req.body.success_url, currency: req.body.currency, callback_url: req.body.callback_url }
            const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
            await axios.post(MOYASAR_API_BASE_URL + "/invoices", data, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': '*/*',
                    'Authorization': `Basic ${encodedApiKey}`
                }
            }).then((response) => {
                res.status(200).json({ success: true, message: "Success", result: { payment: response.data, savedPartnerSubscription }, error: {} })

            })
        } catch (error) {
            console.log(error)
            res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
        }
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
        // res.status(500).json(error)
    }
}

export const generateInvoice = async (req, res, next) => {
    try {

        const data = { amount: req.body.amount, description: req.body.description, success_url: req.body.success_url, currency: req.body.currency, callback_url: req.body.callback_url }
        const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
        await axios.post(MOYASAR_API_BASE_URL + "/invoices", data, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Basic ${encodedApiKey}`
            }
        }).then((response) => {
            res.status(200).json({ success: true, message: "Success", result: response.data, error: {} })

        })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }

}

export const updatePartnerSubscription = async (req, res, next) => {
    try {
        const partner = await Partner.findByIdAndUpdate(req.params.id, { $set: { isSubscribed: true } }, { new: true })
        const updatedPartnerSubscription = await PartnerSubscription.findOneAndUpdate({ partner: req.params.id }, { $set: { subscriptionPayment: req.body } }, { new: true })
        // Update the nextPaymentDate based on the subscription update
        updatedPartnerSubscription.nextPaymentDate = updatedPartnerSubscription.calculateNextPaymentDate();
        await updatedPartnerSubscription.save();
        await updatePartner(req.params.id)
        res.status(200).json({ success: true, message: "Success", result: { updatedPartnerSubscription, partner }, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

// Cron job logic
const updatePartner = async (partnerId) => {
    const currentDateTime = new Date();

    try {
        const subscription = await PartnerSubscription.findOne({
            partner: partnerId
        });

        if (subscription) {
            // Update the specific Partner model - set isSubscribed to false
            await Partner.updateOne({ _id: partnerId }, { $set: { isSubscribed: true } });

            console.log(`Updated partner with ID ${partnerId}.`);
        }
    } catch (error) {
        console.error("Error updating partner:", error);
    }
};

export const deletePartnerSubscription = async (req, res, next) => {
    try {
        await PartnerSubscription.findByIdAndDelete(req.params.id)
        res.status(200).json({ success: true, message: "PartnerSubscription Deleted", result: {}, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getPartnerSubscription = async (req, res, next) => {
    try {
        const partnerSubscription = await PartnerSubscription.findById(req.params.id)
        res.status(200).json({ success: true, message: "Success", result: partnerSubscription, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getSubscriptionOfPartner = async (req, res, next) => {
    try {
        const partnerSubscription = await PartnerSubscription.findOne({ partner: req.params.partnerid })
        if (partnerSubscription) {
            const subscriptionType = await AdminSubscription.findById(partnerSubscription.subscriptionType)
            const partner = await Partner.findById(partnerSubscription.partner)
            res.status(200).json({ success: true, message: "Success", result: { partnerSubscription, partner, subscriptionType }, error: {} })
        } else {
            res.status(200).json({ success: true, message: "Success", result: { partnerSubscription, partner: {}, subscriptionType: {} }, error: {} })
        }

    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getAllPartnerSubscription = async (req, res, next) => {
    try {
        const partnerSubscription = await PartnerSubscription.find()
        res.status(200).json({ success: true, message: "Success", result: partnerSubscription, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
