import Bookings from "../models/Bookings.js"
import Court from "../models/Court.js"
import Customer from "../models/Customer.js"
import FavoriteVenue from "../models/FavoriteVenue.js"
import Partner from "../models/Partner.js"
import PartnerPricing from "../models/PartnerPricing.js"
import Sports from "../models/Sports.js"
import Venue from "../models/Venue.js"
import dotenv from "dotenv"
import axios from "axios"
import SavedPayment from "../models/SavedPayment.js"
import encode from "base-64"
import mongoose from "mongoose"
import fs from 'fs';
import Settlements from "../models/Settlements.js"
import pdfmake from 'pdfmake/build/pdfmake.js'; // Import pdfmake
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
import PDFDocument from "pdfkit"
import AWS from 'aws-sdk'; // Import AWS SDK
dotenv.config();
pdfmake.vfs = pdfFonts.pdfMake.vfs;

const MOYASAR_API_KEY = process.env.MOYASAR_SECRET_KEY; // Replace with your actual API key
const MOYASAR_PUBLISH_API_KEY = process.env.MOYASAR_PUBLISHABLE_KEY; // Replace with your actual API key
const MOYASAR_API_BASE_URL = 'https://api.moyasar.com/v1';
export const createCustomer = async (req, res, next) => {
    const newCustomer = new Customer(req.body)
    try {
        const savedCustomer = await newCustomer.save()
        res.status(200).json({ success: true, message: "Success", result: savedCustomer, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
        // res.status(500).json(error)
    }
}

export const updateCustomer = async (req, res, next) => {
    try {
        const updatedCustomer = await Customer.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
        res.status(200).json({ success: true, message: "Success", result: updatedCustomer, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const deleteCustomer = async (req, res, next) => {
    try {
        await Customer.findByIdAndDelete(req.params.id)
        res.status(200).json({ success: true, message: "Customer Deleted", result: {}, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getCustomer = async (req, res, next) => {
    try {
        const customer = await Customer.findById(req.params.id)
        res.status(200).json({ success: true, message: "Success", result: customer, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getAllCustomers = async (req, res, next) => {
    try {
        const customers = await Customer.find()
        res.status(200).json({ success: true, message: "Success", result: customers, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
const getPricings = async (pricing) => {
    try {
        const partnerPricing = await PartnerPricing.findById(pricing)
        return partnerPricing
    } catch (error) {
        console.log(error)
    }

}
export const getCustomerHomeData = async (req, res, next) => {
    try {
        const venues = await Venue.find()
        const courts = await Court.find()
        const result = { venues, courts }
        res.status(200).json({ success: true, message: "Success", result: result, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerCourts = async (req, res, next) => {
    try {
        const courts = await Court.find({approved:true})
        res.status(200).json({ success: true, message: "Success", result: courts, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerCourtsBySearch = async (req, res, next) => {
    try {
        const regex = new RegExp(req.params.searchterm, 'i');
        const courts = await Court.find({
            $and: [
                {
                    $or: [
                        { title: { $regex: regex } },
                        { courtType: { $regex: regex } },
                    ]
                },
                { approved: true } // Add this condition for filtering by 'approved'
            ]
        })
        res.status(200).json({ success: true, message: "Success", result: courts, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerVenues = async (req, res, next) => {
    try {
        const venues = await Venue.find({approved:true})
        res.status(200).json({ success: true, message: "Success", result: venues, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerVenueDetail = async (req, res, next) => {
    try {
        const venue = await Venue.findById(req.params.venueid)
        res.status(200).json({ success: true, message: "Success", result: venue, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerVenuesBySearch = async (req, res, next) => {
    try {
        const filters = req.body.filter; // Get the filters from the request body
        const cityFilter = req.body.city; // Get the city filter from the request body

        const query = {};

        // Build the query based on the filters
        for (const key in filters) {
            if (filters[key] === 'true') {
                query[`amenities.${key}`] = true;
            }
        }

        if (cityFilter) {
            if (cityFilter === "Anywhere") {
                query.address = { $regex: "", $options: 'i' };
            } else {
                query.address = { $regex: cityFilter, $options: 'i' };
            }

        }

        // Add a condition for 'approved' being equal to true
        query.approved = true;

        const venues = await Venue.find(query); // Find venues based on the query
        const courts = await Court.find()
        const result = { venues, courts }

        res.status(200).json({ success: true, message: "Success", result: result, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const searchVenuesByName = async (req, res, next) => {
    try {
        const searchName = req.body.name; // Get the search name from the query parameter
        // Use a regular expression to search for venues with names that match the searchName with similar characters
        const query = { name: { $regex: searchName, $options: 'i' } };

        // Add a condition for 'approved' being equal to true
        query.approved = true;
        const courts = await Court.find()
        const venues = await Venue.find(query); // Find venues based on the query
        const result = { venues, courts };
        res.status(200).json({ success: true, message: "Success", result: result, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getCustomerPartnerCourtsByVenue = async (req, res, next) => {
    try {
        // const regex = new RegExp(req.params.searchterm, 'i');
        const courts = await Court.find({
            $and: [
                { partner: req.params.partnerid },
                { venue: req.params.venueid },
                { approved: true } // Add this condition for filtering by 'approved'
            ]
        })
        res.status(200).json({ success: true, message: "Success", result: courts, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const getCustomerCourtInfo = async (req, res, next) => {
    try {
        const court = await Court.findById(req.params.courtid)
        res.status(200).json({ success: true, message: "Success", result: court, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}
export const customerCreateBooking = async (req, res, next) => {
    try {
        const data = { amount: req.body.amount * 100, callback_url: req.body.callback_url, currency: req.body.currency, description: req.body.description, source: { type: req.body.source.type, token: req.body.source.token } }
        // const encodedApiKey = encode(MOYASAR_API_KEY)
        const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
        await axios.post(MOYASAR_API_BASE_URL + "/payments/", data, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Basic ${encodedApiKey}`
            }
        })
            .then(async (response) => {

                const newBookings = new Bookings({ ...req.body.bookingData, payment: response.data, status: "Booked" })


                try {
                    const savedBookings = await newBookings.save()
                    const invoiceUrl = await generatepdf(savedBookings)
                    await Bookings.findByIdAndUpdate(savedBookings._id, { $set: { invoice_url: invoiceUrl } })
                    try {
                        await Court.findByIdAndUpdate(req.body.bookingData.court, {
                            $push: { bookingInfo: savedBookings },
                        });
                    } catch (error) {
                        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
                    }
                    try {
                        const checkPartnerSettlementExist = await Settlements.findOne({ partner: req.body.bookingData.partner })
                        if (checkPartnerSettlementExist) {
                            var updatedAmountToSettle = data.amount + checkPartnerSettlementExist.amountToSettle
                            await Settlements.findOneAndUpdate({ partner: req.body.bookingData.partner }, { amountToSettle: updatedAmountToSettle, $push: { bookingsToSettle: savedBookings } })
                        } else {
                            const partnerSettlement = new Settlements({ amountToSettle: data.amount, partner: req.body.bookingData.partner, bookingsToSettle: savedBookings })
                            await partnerSettlement.save()
                        }
                    } catch (error) {
                        res.status(200).json({ success: false, message: "Failed to add settlement", result: {}, error: error })

                    }

                    res.status(200).json({ success: true, message: "Success", result: savedBookings, error: {} })
                } catch (error) {
                    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
                    // res.status(500).json(error)
                }
            }).catch(err => {
                console.log(err)
            })


    } catch (error) {
        console.log(error.data.errors)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }

}

const generatepdf = async (data) => {
    const cusData = await Customer.findById(data.customer)
    const parData = await Partner.findById(data.partner)
    const venueData = await Venue.findById(data.venue)
    const courtData = await Court.findById(data.court)
    try {
        // Create a PDF document
        const doc = new PDFDocument();
    
        generateHeader(doc,parData);
        generateHr(doc,60)
        generateCustomerInformation(doc, data,parData,venueData,courtData,cusData);
        generateInvoiceTable(doc, data,parData,venueData,courtData);
        generateFooter(doc);
    
        // Finalize the PDF
        doc.end();
    
        // Upload PDF to DigitalOcean Spaces
        const spacesEndpoint = process.env.SPACES_END_POINT;
        const accessKeyId = process.env.SPACES_ACCESS_KEY;
        const secretAccessKey = process.env.SPACES_SECRET_ACCESS;
        const spaceName = process.env.SPACE_NAME;
        const s3 = new AWS.S3({
          endpoint: spacesEndpoint,
          accessKeyId: accessKeyId,
          secretAccessKey: secretAccessKey,
        });
    
        const params = {
          Bucket: spaceName,
          Key: `invoices/${data._id}/invoice.pdf`,
          Body: doc,
          ACL: 'public-read',
          ContentType: 'application/pdf',
          ContentDisposition: 'attachment; filename=invoice.pdf',
        };
    
        const uploadResponse = await s3.upload(params).promise();
    
        const pdfUrl = uploadResponse.Location;
        console.log('PDF URL:', pdfUrl);
    
        return pdfUrl;
      } catch (error) {
        console.log('Generate PDF Argument');
        console.error(error);
        return '';
      }
    };
    
    function generateHeader(doc,parData) {
      doc
        .image("courtena-logo.png", 50, 45, { width: 50 })
        .fillColor("#444444")
        // .fontSize(20)
        // .text("ACME Inc.", 110, 57)
        .fontSize(8)
        .text(parData.username, 200, 45, { align: "right" })
        // .text(parData.city, 200, 65, { align: "right" })
        // .text(parData.phone, 200, 80, { align: "right" })
        .moveDown();
    }
    
    function generateCustomerInformation(doc, data,parData,venueData,courtData,cusData) {
      doc
        .fillColor("#444444")
        .fontSize(20)
        .text("Invoice", 50, 100);
    
      generateHr(doc, 125);
    
      const customerInformationTop = 140;
    
      doc
        .fontSize(10)
        .text("Invoice Number:", 50, customerInformationTop)
        .font("Helvetica-Bold")
        .text(data._id, 150, customerInformationTop)
        .font("Helvetica")
        .text("Invoice Date:", 50, customerInformationTop + 15)
        .text(data.createdAt, 150, customerInformationTop + 15)
        .text("Status:", 50, customerInformationTop + 30).fillColor("green")
        .text(data?.payment?.status.toUpperCase(), 85, customerInformationTop + 30).fillColor("black")
        .text(
          data?.payment?.amount_format,
          150,
          customerInformationTop + 30
        )
    
        .font("Helvetica-Bold")
        .text("Customer:", 300, customerInformationTop)
        .font("Helvetica")
        .text(cusData.first_name+" "+cusData.last_name+"("+cusData.phone+")", 350, customerInformationTop)
        // .text(
        //   invoice.shipping.city +
        //     ", " +
        //     invoice.shipping.state +
        //     ", " +
        //     invoice.shipping.country,
        //   300,
        //   customerInformationTop + 30
        // )
        .moveDown();
    
      generateHr(doc, 192);
    }
    
    function generateInvoiceTable(doc, data,parData,venueData,courtData) {
      let i;
      const invoiceTableTop = 230;
    
      doc.font("Helvetica-Bold");
      generateTableRow(
        doc,
        invoiceTableTop,
        "Item",
        "Description",
        "Quantity",
        "Sub Total"
      );
      generateHr(doc, invoiceTableTop + 20);
      doc.font("Helvetica");
      generateTableRow(
        doc,
        invoiceTableTop+30,
        "Reservation",
        courtData?.title+" of "+venueData?.name+" for "+data.duration+" from "+data.dateTimeInfo.timeFrom+"-"+data.dateTimeInfo.timeTo,
        1,
        data?.payment?.amount_format
      );
    
      generateHr(doc, invoiceTableTop + 60);
      generateTableRow(
        doc,
        invoiceTableTop+65,
        "",
        "",
        "",
        "Total "+data?.payment?.amount_format
      );
    
      // generateHr(doc, invoiceTableTop + 90);
    
      doc.font("Helvetica");
    }
    
    function generateFooter(doc) {
      doc
        .fontSize(10)
        .text(
          "Thank you for your business.",
          50,
          580,
          { align: "center", width: 500 }
        );
    }
    
    function generateTableRow(
      doc,
      y,
      item,
      description,
      quantity,
      lineTotal
    ) {
      doc
        .fontSize(10)
        .text(item, 50, y)
        .text(description, 150, y)
        // .text(unitCost, 280, y, { width: 90, align: "right" })
        .text(quantity, 370, y, { width: 90, align: "right" })
        .text(lineTotal, 0, y, { align: "right" });
    }
    
    function generateHr(doc, y) {
      doc
        .strokeColor("#aaaaaa")
        .lineWidth(1)
        .moveTo(50, y)
        .lineTo(550, y)
        .stroke();
    }



export const getCustomerBookings = async (req, res, next) => {
    try {
        const bookings = await Bookings.find({ customer: req.params.customerid }).populate("court venue partner sports")
        res.status(200).json({ success: true, message: "Success", result: bookings, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getCustomerBookingDetail = async (req, res, next) => {
    try {
        const bookings = await Bookings.findById(req.params.bookingid)
        const venue_details = await Venue.findById(bookings.venue)
        const sports = await Sports.findById(bookings.sports)
        const court_details = await Court.findById(bookings.court)
        const partner = await Partner.findById(bookings.partner)
        const result = { bookings, venue_details, sports, court_details, partner }
        res.status(200).json({ success: true, message: "Success", result: result, error: {} })
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const toggleFavVenue = async (req, res, next) => {
    try {
        const customerId = req.body.customer;
        const venueId = req.body.venue;

        const customer = await Customer.findById(customerId);
        if (!customer) {
            return res.status(200).json({ success: false, message: "Customer Not found", result: {}, error: {} })
        }

        // Check if the customer has already favorited the venue
        const existingFavorite = await FavoriteVenue.findOne({
            customer: customerId,
            venue: venueId,
        });

        let isFavorited = false;
        if (existingFavorite) {
            // If the venue is already favorited, remove it from favorites
            await FavoriteVenue.findByIdAndDelete(existingFavorite._id);
        } else {
            // If the venue is not favorited, add it to favorites
            await FavoriteVenue.create({ customer: customerId, venue: venueId, fav: true });
            isFavorited = true;
        }
        res.status(200).json({ success: true, message: "Favorite status toggled successfully", result: isFavorited, error: {} })

    } catch (error) {
        console.error(error);
        res.status(200).json({ success: false, message: "Internal server error", result: {}, error: error })
    }
}

export const getCustomerIsFavVenue = async (req, res, next) => {
    try {
        const customerFavVenue = await FavoriteVenue.findOne({ customer: req.params.customerId, venue: req.params.venueId })
        res.status(200).json({ success: true, message: "Success", result: customerFavVenue, error: {} })

    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })

    }
}

export const getCustomerFavVenues = async (req, res, next) => {
    try {
        const customerFavVenue = await FavoriteVenue.find({ customer: req.params.customerId }).populate("venue")
        res.status(200).json({ success: true, message: "Success", result: customerFavVenue, error: {} })

    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })

    }
}

export const addCustomerCard = async (req, res, next) => {

    try {
        const newPaymentOption = new SavedPayment({ token: req.body.card.id, card: req.body.card, customer: req.body.customer })
        const savedPaymentOption = await newPaymentOption.save()
        res.status(200).json({ success: true, message: "Card Saved", result: savedPaymentOption, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Error Saving Card", result: {}, error: error })
    }
};

export const getSavedCards = async (req, res, next) => {

    try {
        const savedPayments = await SavedPayment.find({ customer: req.params.customerid })
        res.status(200).json({ success: true, message: "Card Saved", result: savedPayments, error: {} })
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Error Saving Card", result: {}, error: error })
    }
};

export const deleteSavedCard = async (req, res, next) => {
    try {
        const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
        await axios.delete(MOYASAR_API_BASE_URL + "/tokens/" + req.params.token, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Basic ${encodedApiKey}`
            }
        }).then(async (res) => {
            await SavedPayment.findOneAndDelete({ customer: req.params.customerid, token: req.params.token });
            res.status(200).json({ success: true, message: "Card Deleted", result: {}, error: {} })
        }).catch(err => {
            res.status(200).json({ success: false, message: "Failure", result: {}, error: err })

        })

    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
}

export const getVerifiedCard = async (req, res, next) => {
    const id = req.query.id;
    const status = req.query.status;
    const amount = req.query.amount;
    const message = req.query.message;

    // Construct the HTML response
    const htmlResponse = `
      <!DOCTYPE html>
      <html>
      <head>
        <style>
          body {
            font-family: Arial, sans-serif;
            text-align: center;
            margin: 100px auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 10px;
            background-color: #f5f5f5;
          }
          h1 {
            color: green;
          }
        </style>
      </head>
      <body>
        <h1>Card Verified</h1>
        <p>ID: ${id}</p>
        <p>Status: ${status}</p>
        <p>Amount: ${amount / 100} SAR</p>
        <p>Message: ${message}</p>
      </body>
      </html>
    `;

    // Send the HTML response
    res.status(200).send(htmlResponse);
    
};

export const cancelVoidCustomerReservation = async (req, res, next) => {
    try {
        const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
        await axios.post(MOYASAR_API_BASE_URL + "/payments/" + req.body.paymentId + "/void", {}, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Basic ${encodedApiKey}`
            }
        })
            .then(async (response) => {
                try {

                    const updatedCourt = await Court.findByIdAndUpdate(req.body.court, {
                        $pull: { bookingInfo: { _id: new mongoose.Types.ObjectId(req.body.bookingId) } },
                    });
                    const updatedBooking = await Bookings.findByIdAndUpdate(req.body.bookingId, {
                        $set: {
                            status: "Canceled", payment: response.data
                        }
                    }, { new: true })

                    res.status(200).json({ success: true, message: "Booking canceled successfully", result: {}, error: {} });
                } catch (error) {
                    console.log(error)
                    res.status(200).json({ success: false, message: "Booking deletion failure", result: {}, error: error });
                }
            }).catch(err => {
                console.log(err)
                res.status(200).json({ success: false, message: "Payment Cancel Error", result: {}, error: err })

            })


    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }

}

export const cancelRefundCustomerReservation = async (req, res, next) => {
    try {
        const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
        await axios.post(MOYASAR_API_BASE_URL + "/payments/" + req.body.paymentId + "/refund", {}, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': '*/*',
                'Authorization': `Basic ${encodedApiKey}`
            }
        })
            .then(async (response) => {
                try {

                    const updatedCourt = await Court.findByIdAndUpdate(req.body.court, {
                        $pull: { bookingInfo: { _id: new mongoose.Types.ObjectId(req.body.bookingId) } },
                    });
                    const updatedBooking = await Bookings.findByIdAndUpdate(req.body.bookingId, {
                        $set: {
                            status: "Refunded", payment: response.data
                        }
                    }, { new: true })


                    res.status(200).json({ success: true, message: "Booking canceled successfully", result: {}, error: {} });
                } catch (error) {
                    console.log(error)
                    res.status(200).json({ success: false, message: "Booking deletion failure", result: {}, error: error });
                }
            }).catch(err => {
                console.log(err)
                res.status(200).json({ success: false, message: "Payment Cancel Error", result: {}, error: err })

            })


    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }

}

export const checkRegisteredPhone = async (req, res, next) => {
    try {
        const customer = await Customer.findOne({ phone: req.body.phone })
        if (customer) {
            res.status(200).json({ success: false, message: "Phone number already registered", result: {}, error: {} })
        } else {
            res.status(200).json({ success: true, message: "Success", result: {}, error: {} })
        }
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })

    }
}

export const checkLoginPhone = async (req, res, next) => {
    try {
        const customer = await Customer.findOne({ phone: req.body.phone })
        if (customer) {
            res.status(200).json({ success: true, message: "Success", result: {}, error: {} })
        } else {
            res.status(200).json({ success: false, message: "This number is not registered", result: {}, error: {} })
        }
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })

    }
}

export const getSettlementsData = async (req, res, next) => {
    try {
        const customer = await Customer.findOne({ phone: req.body.phone })
        if (customer) {
            res.status(200).json({ success: true, message: "Success", result: {}, error: {} })
        } else {
            res.status(200).json({ success: false, message: "This number is not registered", result: {}, error: {} })
        }
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error })

    }
}

export const getApplePayInfo = async (req, res, next) => {
    const info = JSON.parse(req.body.info)
    
    console.log(info.token)
    // const tokenObj = JSON.parse(info.token);
    // console.log(JSON.parse(req.body.token.paymentData))

    const data = { amount: req.body.amount * 100, publishable_api_key:MOYASAR_PUBLISH_API_KEY, currency: req.body.currency, description: req.body.description, source: { type: "applepay", token: info.token.paymentData } }
    // const encodedApiKey = encode(MOYASAR_API_KEY)
    const encodedApiKey = Buffer.from(MOYASAR_API_KEY).toString('base64');
    await axios.post(MOYASAR_API_BASE_URL + "/payments/", data, {
        headers: {
            'Content-Type': 'application/json',
            'Accept': '/',
            'Authorization': `Basic ${encodedApiKey}`
        }
    }).then(async (response) => {

        console.log(response)

        const newBookings = new Bookings({ ...req.body.bookingData, payment: response.data, status: "Booked" })


        try {
            const savedBookings = await newBookings.save()
            const invoiceUrl = await generatepdf(savedBookings)
            await Bookings.findByIdAndUpdate(savedBookings._id, { $set: { invoice_url: invoiceUrl } })
            try {
                await Court.findByIdAndUpdate(req.body.bookingData.court, {
                    $push: { bookingInfo: savedBookings },
                });
            } catch (error) {
                res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
            }
            try {
                const checkPartnerSettlementExist = await Settlements.findOne({ partner: req.body.bookingData.partner })
                if (checkPartnerSettlementExist) {
                    var updatedAmountToSettle = data.amount + checkPartnerSettlementExist.amountToSettle
                    await Settlements.findOneAndUpdate({ partner: req.body.bookingData.partner }, { amountToSettle: updatedAmountToSettle, $push: { bookingsToSettle: savedBookings } })
                } else {
                    const partnerSettlement = new Settlements({ amountToSettle: data.amount, partner: req.body.bookingData.partner, bookingsToSettle: savedBookings })
                    await partnerSettlement.save()
                }
            } catch (error) {
                res.status(200).json({ success: false, message: "Failed to add settlement", result: {}, error: error })

            }

            res.status(200).json({ success: true, message: "Success", result: savedBookings, error: {} })
        } catch (error) {
            res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
            // res.status(500).json(error)
        }
    }).catch(err => {
        console.log(err)
    })
}



