import Bookings from "../models/Bookings.js"
import Court from "../models/Court.js"
import Customer from "../models/Customer.js"
import Partner from "../models/Partner.js"
import PartnerSubscription from "../models/PartnerSubscription.js"
import Settlements from "../models/Settlements.js"
import Venue from "../models/Venue.js"
import moment from "moment/moment.js"
import dotenv from "dotenv"
import pdfmake from 'pdfmake/build/pdfmake.js'; // Import pdfmake
import pdfFonts from 'pdfmake/build/vfs_fonts.js';
import PDFDocument from "pdfkit"
import AWS from 'aws-sdk'; // Import AWS SDK
import PartnerCommission from "../models/PartnerCommission.js"
dotenv.config();
pdfmake.vfs = pdfFonts.pdfMake.vfs;

const MOYASAR_API_KEY = process.env.MOYASAR_SECRET_KEY; // Replace with your actual API key
const MOYASAR_API_BASE_URL = 'https://api.moyasar.com/v1';

export const createPartner = async (req, res, next) => {
  const newPartner = new Partner(req.body)
  try {
    const savedPartner = await newPartner.save()
    res.status(200).json({ success: true, message: "Success", result: savedPartner, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    // res.status(500).json(error)
  }
}

export const updatePartner = async (req, res, next) => {
  try {
    const updatedPartner = await Partner.findByIdAndUpdate(req.params.id, { $set: req.body }, { new: true })
    res.status(200).json({ success: true, message: "Success", result: updatedPartner, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
  }
}

export const deletePartner = async (req, res, next) => {
  try {
    await Partner.findByIdAndDelete(req.params.id)
    res.status(200).json({ success: true, message: "Partner Deleted", result: {}, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
  }
}

export const getPartner = async (req, res, next) => {
  try {
    const partnerSubscription = await PartnerSubscription.findOne({partner:req.params.id})
    const partner = await Partner.findById(req.params.id)
    res.status(200).json({ success: true, message: "Success", result: {partner,partnerSubscription}, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
  }
}

export const getAllPartners = async (req, res, next) => {
  try {
    const partners = await Partner.find()
    res.status(200).json({ success: true, message: "Success", result: partners, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
  }
}

// export const getPartnerCustomers = async (req,res,next) => {
//     try {
//         // const {customer} = await Bookings.find({})
//         const customersids = []
//         const customers = []
//         await Bookings.find({partner:req.params.partnerid}, 'customer')
//             .then((documents) => {
//                 const ids = documents.map((doc) => doc.customer.toString());
//                 const uniqueIds = ids.filter((value, index, self) => {
//                     return self.indexOf(value) === index;
//                   });
//                   const customers = uniqueIds.map(async (item) => {
//                     return await Customer.findById(item)
//                   })
//                   await Promise.all(customers)
//                   console.log(customers)
//                 res.status(200).json({success:true,message:"Success",result:uniqueIds, error:{}})    
//             })
//             .catch((error) => {
//                 console.error('Error retrieving IDs:', error);
//             });


//     } catch (error) {
//         res.status(200).json({success:false,message:"Failure",result:{},error:error})
//     }
// }


export const getPartnerCustomers = async (req, res, next) => {
  try {
    const customersIds = [];
    const customers = [];

    const documents = await Bookings.find({ partner: req.params.partnerid, customer: { $exists: true, $ne: null } }, 'customer');
    const ids = documents.map((doc) => doc.customer);
    const uniqueIds = [...new Set(ids)];

    const customerPromises = uniqueIds.map(async (item) => {
      if (item) {
        // Check if the customer ID is not null or undefined
        return await Customer.findById(item);
      }
      return null;
    });

    const resolvedCustomers = await Promise.all(customerPromises);

    // Filter out duplicates based on customer IDs
    const uniqueResolvedCustomers = resolvedCustomers.filter(
      (customer, index, self) => index === self.findIndex((c) => c && c._id.toString() === customer._id.toString())
    );

    res.status(200).json({ success: true, message: "Success", result: uniqueResolvedCustomers, error: {} });
  } catch (error) {
    console.log(error);
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
  }
};

export const getPartnerBookings = async (req, res, next) => {
  try {
    const bookings = await Bookings.find({ partner: req.params.partnerid })
    const venues = await Venue.find({ partner: req.params.partnerid })
    const courts = await Court.find({ partner: req.params.partnerid })
    const updatedBookings = bookings.map(async (item) => {
      const court = await Court.findById(item.court)
      const venue = await Venue.findById(item.venue)
      const customer = await Customer.findById(item.customer)
      return { booking: item, court, venue, customer }
    })
    const resolvedBookings = await Promise.all(updatedBookings)
    res.status(200).json({ success: true, message: "Success", result: { resolvedBookings, courts, venues }, error: {} });

  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
  }

}

export const manualPartnerBooking = async (req, res, next) => {
  const newBookings = new Bookings({ ...req.body.bookingData, status: "Manually Booked" })

  try {
    const savedBookings = await newBookings.save()
    const invoiceUrl = await generateManualBookingPdf(savedBookings)
    await Bookings.findByIdAndUpdate(savedBookings._id, { $set: { invoice_url: invoiceUrl } })
    try {
      await Court.findByIdAndUpdate(req.body.bookingData.court, {
        $push: { bookingInfo: savedBookings },
      });
    } catch (error) {
      res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
    res.status(200).json({ success: true, message: "Success", result: savedBookings, error: {} });
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
  }
}

const generateManualBookingPdf = async (data) => {
  const parData = await Partner.findById(data.partner);
  const venueData = await Venue.findById(data.venue);
  const courtData = await Court.findById(data.court);

  try {
    // Create a PDF document
    const doc = new PDFDocument();

    generateHeader(doc,parData);
    generateHr(doc,60)
    generateCustomerInformation(doc, data,parData,venueData,courtData);
    generateInvoiceTable(doc, data,parData,venueData,courtData);
    generateFooter(doc);

    // Finalize the PDF
    doc.end();

    // Upload PDF to DigitalOcean Spaces
    const spacesEndpoint = process.env.SPACES_END_POINT;
    const accessKeyId = process.env.SPACES_ACCESS_KEY;
    const secretAccessKey = process.env.SPACES_SECRET_ACCESS;
    const spaceName = process.env.SPACE_NAME;
    const s3 = new AWS.S3({
      endpoint: spacesEndpoint,
      accessKeyId: accessKeyId,
      secretAccessKey: secretAccessKey,
    });

    const params = {
      Bucket: spaceName,
      Key: `invoices/${data._id}/invoice.pdf`,
      Body: doc,
      ACL: 'public-read',
      ContentType: 'application/pdf',
      ContentDisposition: 'attachment; filename=invoice.pdf',
    };

    const uploadResponse = await s3.upload(params).promise();

    const pdfUrl = uploadResponse.Location;
    console.log('PDF URL:', pdfUrl);

    return pdfUrl;
  } catch (error) {
    console.log('Generate PDF Argument');
    console.error(error);
    return '';
  }
};

function generateHeader(doc,parData) {
  doc
    .image("courtena-logo.png", 50, 45, { width: 50 })
    .fillColor("#444444")
    // .fontSize(20)
    // .text("ACME Inc.", 110, 57)
    .fontSize(8)
    .text(parData.username, 200, 45, { align: "right" })
    // .text(parData.city, 200, 65, { align: "right" })
    // .text(parData.phone, 200, 80, { align: "right" })
    .moveDown();
}

function generateCustomerInformation(doc, data,parData,venueData,courtData) {
  doc
    .fillColor("#444444")
    .fontSize(20)
    .text("Invoice", 50, 100);

  generateHr(doc, 125);

  const customerInformationTop = 140;

  doc
    .fontSize(10)
    .text("Invoice Number:", 50, customerInformationTop)
    .font("Helvetica-Bold")
    .text(data._id, 150, customerInformationTop)
    .font("Helvetica")
    .text("Invoice Date:", 50, customerInformationTop + 15)
    .text(data.createdAt, 150, customerInformationTop + 15)
    .text("Status:", 50, customerInformationTop + 30).fillColor("red")
    .text(data?.payment?.status.toUpperCase(), 85, customerInformationTop + 30).fillColor("black")
    .text(
      data?.payment?.amount_format,
      150,
      customerInformationTop + 30
    )

    .font("Helvetica-Bold")
    .text("Customer:", 300, customerInformationTop)
    .font("Helvetica")
    .text(data?.manualContactInfo, 350, customerInformationTop)
    // .text(
    //   invoice.shipping.city +
    //     ", " +
    //     invoice.shipping.state +
    //     ", " +
    //     invoice.shipping.country,
    //   300,
    //   customerInformationTop + 30
    // )
    .moveDown();

  generateHr(doc, 192);
}

function generateInvoiceTable(doc, data,parData,venueData,courtData) {
  let i;
  const invoiceTableTop = 230;

  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    invoiceTableTop,
    "Item",
    "Description",
    "Quantity",
    "Sub Total"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");
  generateTableRow(
    doc,
    invoiceTableTop+30,
    "Reservation",
    courtData?.title+" of "+venueData?.name+" for "+data.duration+" from "+data.dateTimeInfo.timeFrom+"-"+data.dateTimeInfo.timeTo,
    1,
    data?.payment?.amount_format
  );

  generateHr(doc, invoiceTableTop + 60);
  generateTableRow(
    doc,
    invoiceTableTop+65,
    "",
    "",
    "",
    "Total "+data?.payment?.amount_format
  );

  // generateHr(doc, invoiceTableTop + 90);

  doc.font("Helvetica");
}

function generateFooter(doc) {
  doc
    .fontSize(10)
    .text(
      "Pay when you reach venue. Thank you for your business.",
      50,
      580,
      { align: "center", width: 500 }
    );
}

function generateTableRow(
  doc,
  y,
  item,
  description,
  quantity,
  lineTotal
) {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .text(description, 150, y)
    // .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}

function formatCurrency(cents) {
  return "SAR" + (cents / 100).toFixed(2);
}

function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}



export const manualPartnerDeleteBooking = async (req, res, next) => {
  try {
    await Bookings.findByIdAndDelete(req.params.bookingid)
    res.status(200).json({ success: true, message: "Booking Deleted", result: {}, error: {} })
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
  }
}
export const manualPartnerBlockBooking = async (req, res, next) => {
  const newBookings = new Bookings({ ...req.body.bookingData, status: "Manually Booked" })

  try {
    const savedBookings = await newBookings.save()
    try {
      await Court.findByIdAndUpdate(req.body.bookingData.court, {
        $push: { bookingInfo: savedBookings },
      });
    } catch (error) {
      res.status(200).json({ success: false, message: "Failure", result: {}, error: error })
    }
    res.status(200).json({ success: true, message: "Success", result: savedBookings, error: {} });
  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
  }
}

export const updateSchema = async (req, res, next) => {
  try {
    // Find all documents that don't have the new field
    const documentsToUpdate = await Bookings.find({ manualBooking: { $exists: false }, manualContactInfo: { $exists: false }, blockBooking: { $exists: false }, blockTimeInfo: { $exists: false } });

    // Update each document with the new field
    for (const document of documentsToUpdate) {
      document.manualBooking = false;
      document.blockBooking = false;
      document.blockTimeInfo = "test";
      document.manualContactInfo = "test";
      await document.save();
    }

    console.log('Schema update completed.');
  } catch (error) {
    console.error('Error updating schema:', error);
  } finally {
    mongoose.disconnect();
  }
}

export const getPartnerSettlements = async (req, res, next) => {
  try {
    const settlement = await Settlements.findOne({ partner: req.params.partnerid })
    const commission = await PartnerCommission.findOne({partner:req.params.partnerid})
    res.status(200).json({ success: true, message: "Success", result: {settlement,commission}, error: {} });

  } catch (error) {
    res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
  }

}

export const getPartnerDashboardData = async (req, res, next) => {
  try {
    const documents = await Bookings.find({ partner: req.params.partnerid }, 'customer date dateTimeInfo court');
    let customerCount = documents.length > 0 ? documents.length : 0;

    // Initialize default values for statistics
    let maleCount = 0;
    let femaleCount = 0;
    let majorityAgeRange = "--";
    let averageAge = "None";
    let dayOfWeekWithMostBookings = "No Bookings";
    let peakTimeRangeWithMostBookings = "No Bookings";
    let busiestCourt = "None";

    if (customerCount > 0) {
      const ids = documents.map((doc) => doc.customer);
      const uniqueIds = [...new Set(ids)];

      const customerPromises = uniqueIds.map(async (item) => {
        return await Customer.findById(item, 'gender dob');
      });

      const resolvedCustomers = await Promise.all(customerPromises);

      const currentDate = moment(); // Get the current date

      // Define age ranges
      const ageRanges = {
        '<18': { minAge: 0, maxAge: 17 },
        '18to35': { minAge: 18, maxAge: 35 },
        '36to50': { minAge: 36, maxAge: 50 },
        '>50': { minAge: 51, maxAge: 100 },
      };

      const ageRangeCounts = {}; // Initialize age range counts

      resolvedCustomers.forEach((customer) => {
        if (customer && customer.gender) {
          if (customer.gender.toLowerCase() === 'male') {
            maleCount++;
          } else if (customer.gender.toLowerCase() === 'female') {
            femaleCount++;
          }
        }

        if (customer && customer.dob) {
          const dob = moment(customer.dob, 'YYYY/MM/DD'); // Parse the DOB
          const age = currentDate.diff(dob, 'years'); // Calculate age

          // Determine the age range for the customer
          for (const key in ageRanges) {
            const { minAge, maxAge } = ageRanges[key];
            if (age >= minAge && age <= maxAge) {
              // Initialize the count if it doesn't exist
              if (!ageRangeCounts[key]) {
                ageRangeCounts[key] = [];
              }
              ageRangeCounts[key].push(age);
              break; // Exit loop once the age range is found
            }
          }
        }
      });

      // Find the age range with the majority of customers
      let majorityAgeRangeCount = 0;

      for (const key in ageRangeCounts) {
        const ageCount = ageRangeCounts[key].length;
        if (ageCount > majorityAgeRangeCount) {
          majorityAgeRangeCount = ageCount;
          majorityAgeRange = key;
        }
      }

      // Calculate the average age for the majority age range
      const majorityAges = ageRangeCounts[majorityAgeRange];
      averageAge =
        majorityAges.reduce((sum, age) => sum + age, 0) / majorityAges.length;
    }

    customerCount = maleCount + femaleCount;
    const malePercentage = (maleCount / customerCount) * 100;
    const femalePercentage = (femaleCount / customerCount) * 100;

    if (documents.length > 0) {
      // Calculate the day of the week with the most bookings
      dayOfWeekWithMostBookings = getDayOfWeekWithMostBookings(documents);

      // Calculate the peak time range with the most bookings
      peakTimeRangeWithMostBookings = getPeakTimeRangeWithMostBookings(documents);

      // Calculate the busiest court (court with the most bookings)
      const courtCounts = {};

      documents.forEach((booking) => {
        const courtId = booking.court;
        if (!courtCounts[courtId]) {
          courtCounts[courtId] = 0;
        }
        courtCounts[courtId]++;
      });

      // Find the busiest court
      let busiestCourtBookings = 0;

      for (const courtId in courtCounts) {
        const courtBookingCount = courtCounts[courtId];
        if (courtBookingCount > busiestCourtBookings) {
          busiestCourtBookings = courtBookingCount;
          busiestCourt = courtId;
        }
      }

      // Get the title of the busiest court
      if (busiestCourt !== "None") {
        busiestCourt = await getCourtTitleById(busiestCourt);
      }
    }

    const settlement = await Settlements.findOne({ partner: req.params.partnerid })
    const commission = await PartnerCommission.findOne({partner:req.params.partnerid})

    res.status(200).json({
      success: true,
      message: "Success",
      result: {
        customerCount,
        bookingCount: documents.length,
        malePercentage,
        femalePercentage,
        majorityAgeRange,
        averageAge,
        dayOfWeekWithMostBookings,
        peakTimeRangeWithMostBookings,
        busiestCourt,
        settlement,
        commission
      },
      error: {},
    });
  } catch (error) {
    console.log(error);
    res.status(200).json({
      success: false,
      message: "Failure",
      result: {},
      error: error,
    });
  }
};

// Helper function to calculate the day of the week with the most bookings
function getDayOfWeekWithMostBookings(bookings) {
  const dayOfWeekCounts = {};

  bookings.forEach((booking) => {
    const date = moment(booking.date, 'YYYY/MM/DD');
    const dayOfWeek = date.format('dddd'); // Get the day of the week as a string

    if (!dayOfWeekCounts[dayOfWeek]) {
      dayOfWeekCounts[dayOfWeek] = 0;
    }
    dayOfWeekCounts[dayOfWeek]++;
  });

  // Find the day of the week with the most bookings
  let mostBookedDay = '';
  let mostBookingsCount = 0;

  for (const dayOfWeek in dayOfWeekCounts) {
    const bookingsCount = dayOfWeekCounts[dayOfWeek];
    if (bookingsCount > mostBookingsCount) {
      mostBookingsCount = bookingsCount;
      mostBookedDay = dayOfWeek;
    }
  }

  return mostBookedDay;
}

// Helper function to calculate the peak time range with the most bookings
function getPeakTimeRangeWithMostBookings(bookings) {
  const timeRangeCounts = {};

  bookings.forEach((booking) => {
    const { timeFrom, timeTo } = booking.dateTimeInfo;

    // Convert timeFrom and timeTo to moment objects
    const startTime = moment(timeFrom, 'HH:mm');
    const endTime = moment(timeTo, 'HH:mm');

    // Calculate the time range for the booking
    const timeRange = startTime.format('HH:mm') + ' - ' + endTime.format('HH:mm');

    if (!timeRangeCounts[timeRange]) {
      timeRangeCounts[timeRange] = 0;
    }
    timeRangeCounts[timeRange]++;
  });

  // Find the time range with the most bookings
  let mostBookedTimeRange = '';
  let mostBookingsCount = 0;

  for (const timeRange in timeRangeCounts) {
    const bookingsCount = timeRangeCounts[timeRange];
    if (bookingsCount > mostBookingsCount) {
      mostBookingsCount = bookingsCount;
      mostBookedTimeRange = timeRange;
    }
  }

  return mostBookedTimeRange;
}

// Helper function to get the court title by court ID
async function getCourtTitleById(courtId) {
  try {
    const court = await Court.findById(courtId, 'title');
    return court.title;
  } catch (error) {
    console.log(error);
    return '';
  }
}



