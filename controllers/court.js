import mongoose from "mongoose"
import Court from "../models/Court.js"
import Partner from "../models/Partner.js"
import Venue from "../models/Venue.js"
import CourtReview from "../models/CourtReview.js"
export const createCourt = async (req,res,next) => {
    const filePath = req.file.location
    const venueId = req.params.venueId
    const newCourt = new Court({...req.body,image:filePath,img_path:req.file.path,courtFeature:JSON.parse(req.body.courtFeature),advancedSettings:JSON.parse(req.body.advancedSettings)})
    
    try {
        const savedCourt = await newCourt.save()
        try {
            await Venue.findByIdAndUpdate(venueId,{
                $push:{courts:savedCourt._id},
            });
        } catch (error) {
            res.status(200).json({success:false,message:"Something Went Wrong",result:{},error:error})
        }
        // res.status(200).json({success:true,message:"Your court is created and sent for review!",result:savedCourt, error:{}})    
        res.status(200).json({success:true,message:"Your court is created!",result:savedCourt, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Something Went Wrong",result:{},error:error})
        // res.status(500).json(error)
    }
}

export const updateCourt = async (req,res,next) => {
    try {
        if(req.file){
            const filePath = req.file.location
            const updatedCourt = await Court.findByIdAndUpdate(req.params.id, {$set: {...req.body,image:filePath,img_path:req.file.path,courtFeature:JSON.parse(req.body.courtFeature),advancedSettings:JSON.parse(req.body.advancedSettings),in_review:false}},{new:true})
            // const courtReview = new CourtReview({
            //     courtId:req.params.id,
            //     editedFields:{...req.body,image:filePath,img_path:req.file.path,courtFeature:JSON.parse(req.body.courtFeature),advancedSettings:JSON.parse(req.body.advancedSettings)},
            // })
            // const savedCourtReview = await courtReview.save()
            // res.status(200).json({success:true,message:"Your court updates has been sent for review!",result:updatedCourt, error:{}}) 
            res.status(200).json({success:true,message:"Your court is updated!",result:updatedCourt, error:{}}) 
        }else{
            const updatedCourt = await Court.findByIdAndUpdate(req.params.id, {$set: {...req.body,courtFeature:JSON.parse(req.body.courtFeature),advancedSettings:JSON.parse(req.body.advancedSettings),in_review:false}},{new:true})
            // const courtReview = new CourtReview({
            //     courtId:req.params.id,
            //     editedFields:{...req.body,courtFeature:JSON.parse(req.body.courtFeature),advancedSettings:JSON.parse(req.body.advancedSettings)}
            // })
            // const savedCourtReview = await courtReview.save()
            // res.status(200).json({success:true,message:"Your court updates has been sent for review!",result:updatedCourt, error:{}}) 
            res.status(200).json({success:true,message:"Your court is updated!",result:updatedCourt, error:{}}) 
        }
   
    } catch (error) {
        res.status(200).json({success:false,message:"Something went wrong",result:{},error:error})
    }
}

export const deleteCourt = async (req,res,next) => {

    try {
        await Court.findByIdAndDelete(req.params.id)
        try {
            await Venue.findByIdAndUpdate(req.params.venueId,{
                $pull:{courts:req.params.id},
            })
        } catch (error) {
            res.status(200).json({success:false,message:"Failure",result:{},error:error})
        }
        res.status(200).json({success:true,message:"Court Deleted",result:{}, error:{}})     
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getCourt = async (req,res,next) => {
    try {
        const court = await Court.findById(req.params.id)
        res.status(200).json({success:true,message:"Success",result:court, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAllCourts = async (req,res,next) => {
    try {
        const courts = await Court.find()
        res.status(200).json({success:true,message:"Success",result:courts, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAllCourtsByPartner = async (req,res,next) => {
    try {
        const partner = await Partner.findById(req.params.partnerid)
        const courts = await Court.find({partner:req.params.partnerid})
        const resData = {partner,courts}
        res.status(200).json({success:true,message:"Success",result:resData, error:{}})    
    } catch (error) {
        console.log(error.message)
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}