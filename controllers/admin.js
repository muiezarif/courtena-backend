import Admin from "../models/Admin.js"
import Bookings from "../models/Bookings.js"
import Court from "../models/Court.js"
import CourtReview from "../models/CourtReview.js"
import Customer from "../models/Customer.js"
import Partner from "../models/Partner.js"
import PartnerBillingInformation from "../models/PartnerBillingInformation.js"
import PartnerCommission from "../models/PartnerCommission.js"
import Settlements from "../models/Settlements.js"
import Sports from "../models/Sports.js"
import Venue from "../models/Venue.js"
import VenueReview from "../models/VenueReview.js"
export const createAdmin = async (req,res,next) => {
    const newAdmin = new Admin(req.body)
    try {
        const savedAdmin = await newAdmin.save()
        res.status(200).json({success:true,message:"Success",result:savedAdmin, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
        // res.status(500).json(error)
    }
}

export const addCategory = async (req,res,next) => {
    const newCategory = new Admin(req.body)
    try {
        const savedCategory = await newCategory.save()
        res.status(200).json({success:true,message:"Success",result:savedCategory, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
        // res.status(500).json(error)
    }
}

export const updateAdmin = async (req,res,next) => {
    try {
        const updatedAdmin = await Admin.findByIdAndUpdate(req.params.id, {$set: req.body},{new:true})
        res.status(200).json({success:true,message:"Success",result:updatedAdmin, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const deleteAdmin = async (req,res,next) => {
    try {
        await Admin.findByIdAndDelete(req.params.id)
        res.status(200).json({success:true,message:"Admin Deleted",result:{}, error:{}})     
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAdmin = async (req,res,next) => {
    try {
        const admin = await Admin.findById(req.params.id)
        res.status(200).json({success:true,message:"Success",result:admin, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAllAdmins = async (req,res,next) => {
    try {
        const admins = await Admin.find()
        res.status(200).json({success:true,message:"Success",result:admins, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getPartnerVenues = async (req,res,next) => {
    try {
        const admin = await Venue.find({partner:req.params.partnerId})
        res.status(200).json({success:true,message:"Success",result:admin, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getPartnerCourts = async (req,res,next) => {
    try {
        const admin = await Court.find({partner:req.params.partnerId})
        res.status(200).json({success:true,message:"Success",result:admin, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAdminCustomers = async (req,res,next) => {
    try {
        const customers = await Customer.find()
        res.status(200).json({success:true,message:"Success",result:customers, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAdminBookings = async (req,res,next) => {
    try {
        const bookings = await Bookings.find()
        const updatedBookings = bookings.map(async (item) => {
            const court = await Court.findById(item.court)
            const venue = await Venue.findById(item.venue)
            const customer = await Customer.findById(item.customer)
            const partner = await Partner.findById(item.partner)
            return {booking:item,court,venue,customer,partner}
        })
        const resolvedBookings = await Promise.all(updatedBookings)
        res.status(200).json({success:true,message:"Success",result:resolvedBookings, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getPartnerCommission = async (req, res, next) => {
    try {
        const { partnerid } = req.params;
        const partnerCommission = await PartnerCommission.findOne({ partner: partnerid });

        if (!partnerCommission) {
            return res.status(200).json({success:false, message: "Partner commission not found.",result:{},error:{} });
        }

        return res.status(200).json({success:true,message:"Success",result:partnerCommission,error:{}});
    } catch (error) {
        console.error("Error fetching partner commission:", error);
        res.status(200).json({success:false,message:"Failure",result:{},error:error})    }
};

export const updatePartnerCommission = async (req, res, next) => {
    try {
        const { partnerid } = req.params;
        const { commission } = req.body;

        // Check if partner commission exists
        const existingPartnerCommission = await PartnerCommission.findOne({ partner: partnerid });

        if (existingPartnerCommission) {
            // Update existing partner commission
            const updatedCommission = await PartnerCommission.findOneAndUpdate(
                { partner: partnerid },
                { commission: commission },
                { new: true }
            );

            return res.status(200).json({success:true,message:"Success",result:updatedCommission,error:{}});
        } else {
            // Create new partner commission
            const newPartnerCommission = new PartnerCommission({
                commission: commission,
                partner: partnerid,
            });

            await newPartnerCommission.save();
            return res.status(200).json({success:true,message:"Success",result:newPartnerCommission,error:{}});
        }
    } catch (error) {
        console.error("Error updating partner commission:", error);
        res.status(200).json({success:false,message:"Failure",result:{},error:error})    }
};



export const getAdminReviewsCount = async (req,res,next) => {
    try {
        const venue_review_count = await Venue.countDocuments({ in_review: true });
        const court_review_count = await Court.countDocuments({ in_review: true });
        const result = {venue_review_count,court_review_count}
        res.status(200).json({success:true,message:"Success",result:result, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAdminVenueInReview = async (req, res, next) => {
    try {
        const venuesInReview = await Venue.find({ in_review: true }).populate("partner");
        res.status(200).json({ success: true, message: "Success", result: venuesInReview, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const getAdminVenueInReviewDetails = async (req, res, next) => {
    try {
        const venue = await Venue.findById(req.params.venueid).populate("partner");
        res.status(200).json({ success: true, message: "Success", result: venue, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const getAdminVenueDetails = async (req, res, next) => {
    try {
        const venue = await Venue.findById(req.params.venueid).populate("partner");
        res.status(200).json({ success: true, message: "Success", result: venue, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const getAdminEditVenueInReviewDetails = async (req, res, next) => {
    try {
        const venue = await VenueReview.findOne({venueId:req.params.venueid});
        res.status(200).json({ success: true, message: "Success", result: venue, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const approveFirstTimeVenueReview = async (req,res,next) => {
    try {
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {approved:true,in_review:false}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const approveVenue = async (req,res,next) => {
    try {
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {approved:true}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const disapproveVenue = async (req,res,next) => {
    try {
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {approved:false}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const approveEditVenueReview = async (req,res,next) => {
    try {
        const venue = await VenueReview.findOne({venueId:req.body.data.venueId});
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: venue.editedFields},{new:true})
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {in_review:false,approved:true}},{new:true})
        await VenueReview.findOneAndDelete({venueId:req.body.data.venueId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const rejectFirstTimeVenueReview = async (req,res,next) => {
    try {
        // await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {approved:false,in_review:false}},{new:true})
        await Venue.findByIdAndDelete(req.body.data.venueId)
        await VenueReview.findOneAndDelete({venueId:req.body.data.venueId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const rejectEditVenueReview = async (req,res,next) => {
    try {
        await Venue.findByIdAndUpdate(req.body.data.venueId,{$set: {in_review:false}},{new:true})
        await VenueReview.findOneAndDelete({venueId:req.body.data.venueId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }    
}

export const getAdminCourtInReview = async (req, res, next) => {
    try {
        const courtsInReview = await Court.find({ in_review: true }).populate("partner");
        res.status(200).json({ success: true, message: "Success", result: courtsInReview, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}



export const getAdminCourtInReviewDetails = async (req, res, next) => {

    try {
        const court = await Court.findById(req.params.courtid);

        res.status(200).json({ success: true, message: "Success", result: court, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const getAdminCourtDetails = async (req, res, next) => {

    try {
        const court = await Court.findById(req.params.courtid);

        res.status(200).json({ success: true, message: "Success", result: court, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}

export const getAdminEditCourtInReviewDetails = async (req, res, next) => {

    try {
        const court = await CourtReview.findOne({courtId:req.params.courtid});
        res.status(200).json({ success: true, message: "Success", result: court, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
}



export const approveFirstTimeCourtReview = async (req,res,next) => {
    try {
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {approved:true,in_review:false}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const approveCourt = async (req,res,next) => {
    try {
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {approved:true}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const disapproveCourt = async (req,res,next) => {
    try {
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {approved:false}},{new:true})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const approveEditCourtReview = async (req,res,next) => {
    try {
        const court = await CourtReview.findOne({courtId:req.body.data.courtId});
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: court.editedFields},{new:true})
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {in_review:false,approved:true}},{new:true})
        await CourtReview.findOneAndDelete({courtId:req.body.data.courtId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const rejectFirstTimeCourtReview = async (req,res,next) => {
    try {
        // await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {approved:false,in_review:false}},{new:true})
        await Court.findByIdAndDelete(req.body.data.courtId)
        await CourtReview.findOneAndDelete({courtId:req.body.data.courtId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
}

export const rejectEditCourtReview = async (req,res,next) => {
    try {
        await Court.findByIdAndUpdate(req.body.data.courtId,{$set: {in_review:false}},{new:true})
        await CourtReview.findOneAndDelete({courtId:req.body.data.courtId})
        res.status(200).json({ success: true, message: "Success", result: {}, error: {} });
    } catch (error) {
        console.log(error)
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }    
}

export const getPartnerBookings = async(req, res, next) => {
    try {
        const bookings = await Bookings.find({partner:req.params.partnerid})
        const updatedBookings = bookings.map(async (item) => {
            const court = await Court.findById(item.court)
            const venue = await Venue.findById(item.venue)
            const customer = await Customer.findById(item.customer)
            return {booking:item,court,venue,customer}
        })
        const resolvedBookings = await Promise.all(updatedBookings)
        res.status(200).json({ success: true, message: "Success", result: resolvedBookings, error: {} });

    } catch (error) {
        res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
    
  }

  export const getAllSettlements = async (req, res, next) => {
    try {
      // Get all settlements and populate the "partner" field
      const settlements = await Settlements.find().populate("partner");
  
      // Debugging: Log settlements data
      console.log("Settlements:", settlements);
  
      // Fetch partner commissions to include in settlements data
      const partnerIds = settlements.map(settlement => settlement.partner._id);
      console.log("Partner IDs:", partnerIds);
  
      const partnerCommissions = await PartnerCommission.find({ partner: { $in: partnerIds } });
      console.log("Partner Commissions:", partnerCommissions);
  
      // Add partnerCommission to each settlement
      settlements.forEach((settlement) => {
        const partnerId = settlement.partner._id.toString();
        const partnerCommission = partnerCommissions.find(commission => commission.partner.toString() === partnerId);
        settlement.partnerCommission = partnerCommission ? partnerCommission.commission : 0;
        // Calculate amountToSettleAfterCommission and amountSettledAfterCommission
      settlement.amountToSettleAfterCommission = settlement.amountToSettle - (settlement.amountToSettle * (settlement.partnerCommission/100));
      settlement.amountSettledAfterCommission = settlement.amountSettled - (settlement.amountSettled * (settlement.partnerCommission/100));
        // Debugging: Log settlement with partnerCommission
        console.log("Settlement with Partner Commission:", settlement.partnerCommission);
      });
  
      // Debugging: Log settlements data after adding partnerCommission
      console.log("Settlements with Partner Commission:", settlements);
  
      // Calculate total sum of amountToSettle and amountSettled
      const totalAmountToSettle = settlements.reduce((sum, settlement) => sum + settlement.amountToSettle, 0);
      const totalAmountSettled = settlements.reduce((sum, settlement) => sum + settlement.amountSettled, 0);
  
      // Calculate total payments by adding amountToSettle and amountSettled
      const totalPayments = totalAmountToSettle + totalAmountSettled;

          // Calculate total sum of amountToSettle and amountSettled after commission
    const totalAmountToSettleWithCommission = settlements.reduce(
        (sum, settlement) => sum + settlement.amountToSettleAfterCommission,
        0
      );
      const totalAmountSettledWithCommission = settlements.reduce(
        (sum, settlement) => sum + settlement.amountSettledAfterCommission,
        0
      );
  
      // Calculate total payments by adding amountToSettleAfterCommission and amountSettledAfterCommission
      const totalPaymentsWithCommission = totalAmountToSettleWithCommission + totalAmountSettledWithCommission;
  
      // Prepare the response data
      const responseData = {
        settlements,
        totalAmountToSettle,
        totalAmountSettled,
        totalPayments,
        totalAmountToSettleWithCommission,
        totalAmountSettledWithCommission,
        totalPaymentsWithCommission
      };
  
      res.status(200).json({ success: true, message: "Success", result: responseData, error: {} });
    } catch (error) {
      res.status(500).json({ success: false, message: "Failure", result: {}, error: error });
    }
  };

  export const settlePaymentToPartner = async (req, res, next) => {
    try {
      const { settlementid } = req.params; // Get the settlementid from request parameters
  
      // Find the Settlement document by ID
      const settlement = await Settlements.findById(settlementid);
  
      if (!settlement) {
        return res.status(404).json({ success: false, message: "Settlement not found" });
      }
  
      // Update fields
      settlement.amountSettled += settlement.amountToSettle; // Add amountToSettle to amountSettled
      settlement.settledBookings.push(...settlement.bookingsToSettle); // Add bookingsToSettle to settledBookings
      settlement.amountToSettle = 0; // Set amountToSettle to default (0)
      settlement.bookingsToSettle = []; // Set bookingsToSettle to default (empty array)
  
      // Save the updated Settlement document
      await settlement.save();
  
      // Send success response
      res.status(200).json({ success: true, message: "Payment settled successfully", result: settlement,error:{} });
    } catch (error) {
      console.log(error);
      res.status(200).json({ success: false, message: "Failure", result: {}, error: error });
    }
  };

  export const getPartnerBillingInfo = async (req, res, next) => {
    try {
      const billingInfo = await PartnerBillingInformation.findOne({ partner: req.params.partnerid });
  
      if (!billingInfo) {
        return res.status(200).json({ success: false, message: "Billing information not added by this partner yet",result:{},error:{} });
      }
  
      // Send the billingInfo object as part of the response
      res.status(200).json({ success: true, message: "Success", result: billingInfo, error: null });
    } catch (error) {
      console.log(error);
      res.status(500).json({ success: false, message: "Failure", result: {}, error: error });
    }
  };

