import Court from "../models/Court.js"
import Partner from "../models/Partner.js"
import Venue from "../models/Venue.js"
import VenueReview from "../models/VenueReview.js"
export const createVenue = async (req,res,next) => {
    var newPhotos = []
    if(req.files){
        req.files.map(item =>{
            newPhotos.push(item.location)
        })
    }
    
    const data = {
        name:req.body.name,
        city:req.body.city,
        address:req.body.address,
        photos:newPhotos,
        district:req.body.district,
        description:req.body.description,
        cheapestPrice:req.body.cheapestPrice,
        venuePhone:req.body.venuePhone,
        postalCode:req.body.postalCode,
        partner:req.body.partner,
        amenities:JSON.parse(req.body.amenities),
        timing:JSON.parse(req.body.timing),
    }
    const newVenue = new Venue(data)
    try {
        const savedVenue = await newVenue.save()
        // res.status(200).json({success:true,message:"Your venue is created and sent for review!",result:savedVenue, error:{}})    
        res.status(200).json({success:true,message:"Your venue is created!",result:savedVenue, error:{}})    
    } catch (error) {
        console.log(error)
        res.status(200).json({success:false,message:"Something Went Wrong",result:{},error:error})
    }
}

export const updateVenue = async (req,res,next) => {
    try {
        if(req.files){
            var newPhotos = []
            req.files.map(item =>{
                newPhotos.push(item.location)
            })
            const editingVenue = await Venue.findById(req.params.id)
            const data = {
                name:req.body.name,
                city:req.body.city,
                district:req.body.district,
                address:req.body.address,
                photos:newPhotos.length > 0? newPhotos : editingVenue.photos,
                description:req.body.description,
                cheapestPrice:req.body.cheapestPrice,
                venuePhone:req.body.venuePhone,
                postalCode:req.body.postalCode,
                partner:req.body.partner,
                amenities:JSON.parse(req.body.amenities),
                timing:JSON.parse(req.body.timing),
                in_review:false,
                // approved:true
            }
            // const updatedVenue = await Venue.findByIdAndUpdate(req.params.id, {$set: data},{new:true})
            const updatedVenue = await Venue.findByIdAndUpdate(req.params.id, {$set: {...data,in_review:false}},{new:true})
            // const venueReview = new VenueReview({
            //     venueId:req.params.id,
            //     editedFields:data,
            // })
            // const savedVenueReview = await venueReview.save()
            // res.status(200).json({success:true,message:"Your venue updates has been sent for review!",result:updatedVenue, error:{}})  
            res.status(200).json({success:true,message:"Your venue is updated!",result:updatedVenue, error:{}})  
        }else{
            const editingVenue = await Venue.findById(req.params.id)
            const data = {
                name:req.body.name,
                city:req.body.city,
                district:req.body.district,
                address:req.body.address,
                description:req.body.description,
                cheapestPrice:req.body.cheapestPrice,
                venuePhone:req.body.venuePhone,
                postalCode:req.body.postalCode,
                partner:req.body.partner,
                photos:editingVenue.photos,
                amenities:JSON.parse(req.body.amenities),
                timing:JSON.parse(req.body.timing),
                in_review:false,
                // approved:true
            }
            // const updatedVenue = await Venue.findByIdAndUpdate(req.params.id, {$set: data},{new:true})
            const updatedVenue = await Venue.findByIdAndUpdate(req.params.id, {$set: {...data,in_review:false}},{new:true})
            // const venueReview = new VenueReview({
            //     venueId:req.params.id,
            //     editedFields:data,
            // })
            // const savedVenueReview = await venueReview.save()
            // res.status(200).json({success:true,message:"Your venue updates has been sent for review!",result:updatedVenue, error:{}})  
            res.status(200).json({success:true,message:"Your venue is updated!",result:updatedVenue, error:{}})  
        }
  
    } catch (error) {
        res.status(200).json({success:false,message:"Something Went Wrong",result:{},error:error})
    }
}

export const deleteVenue = async (req,res,next) => {
    try {
        await Court.deleteMany({venue:req.params.id})
        await Venue.findByIdAndDelete(req.params.id)
        res.status(200).json({success:true,message:"Venue Deleted",result:{}, error:{}})     
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getVenue = async (req,res,next) => {
    try {
        const venue = await Venue.findById(req.params.id)
        res.status(200).json({success:true,message:"Success",result:venue, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAllVenues = async (req,res,next) => {
    try {
        const venues = await Venue.find()
        res.status(200).json({success:true,message:"Success",result:venues, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}

export const getAllVenuesByPartner = async (req,res,next) => {
    try {
        const partner = await Partner.findById(req.params.partnerid)
        const venues = await Venue.find({partner:req.params.partnerid})
        const resData = {partner,venues}
        res.status(200).json({success:true,message:"Success",result:resData, error:{}})    
    } catch (error) {
        res.status(200).json({success:false,message:"Failure",result:{},error:error})
    }
}