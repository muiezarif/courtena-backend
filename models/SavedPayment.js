import mongoose from "mongoose";

const {Schema} = mongoose

const SavedPaymentSchema = new mongoose.Schema(
{
    token:{
        type:String,
        required:true
    },
    card:{
        type:Object,
        required:true
    },
    customer:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Customer'
    },

},
{
    timestamps:true
});

export default mongoose.model("SavedPayment",SavedPaymentSchema)