import mongoose from "mongoose";

const {Schema} = mongoose

const CustomerSchema = new mongoose.Schema(
{
    first_name:{
        type:String,
    },
    last_name:{
        type:String,
    },
    gender:{
        type:String,
    },
    dob:{
        type:String,
    },
    username:{
        type:String
    },
    email:{
        type:String,
    },
    country:{
        type:String
    },
    img:{
        type:String
    },
    guest:{
        type:Boolean,
        default:false
    },
    city:{
        type:String
    },
    isFirstTime:{
        type:Boolean,
        default:false
    },
    phone:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true
    }
},
{
    timestamps:true
});

export default mongoose.model("Customer",CustomerSchema)