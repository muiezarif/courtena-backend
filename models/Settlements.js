import mongoose from "mongoose";

const {Schema} = mongoose

const SettlementsSchema = new mongoose.Schema(
{
    amountToSettle:{
        type:Number,
        required:true,
        default:0
    },
    amountToSettleAfterCommission:{
        type:Number,
        default:0
    },
    amountSettled:{
        type:Number,
        default:0
    },
    amountSettledAfterCommission:{
        type:Number,
        default:0
    },
    bookingsToSettle:{
        type:[Object]
    },
    settledBookings:{
        type:[Object]
    },
    partnerCommission: {  // Add partnerCommission field
        type: Number,
        default: 0,
      },
    partner:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Partner'
    },

},
{
    timestamps:true
});

export default mongoose.model("Settlements",SettlementsSchema)