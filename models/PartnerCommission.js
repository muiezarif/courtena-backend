import mongoose from "mongoose";

const {Schema} = mongoose

const PartnerCommissionSchema = new mongoose.Schema({
    commission:{
        type:Number,
        required:true
    },
    partner:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Partner'
    }
},
{
    timestamps:true
})

export default mongoose.model("PartnerCommission",PartnerCommissionSchema)