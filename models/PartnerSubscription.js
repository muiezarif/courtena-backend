import mongoose from "mongoose";

const {Schema} = mongoose

const PartnerSubscriptionSchema = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    subscriptionType:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'AdminSubscription'
    },
    partner:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Partner'
    },
    subscriptionPayment:{
        type:Object
    },
    nextPaymentDate:{
        type:String
    }
},
{
    timestamps:true
})


// Calculate the next payment date based on the last update time
PartnerSubscriptionSchema.methods.calculateNextPaymentDate = function () {
    const lastUpdatedAt = this.updatedAt || this.createdAt;
    const lastPaymentDate = new Date(lastUpdatedAt);
    const nextPaymentDate = new Date(lastPaymentDate.setMonth(lastPaymentDate.getMonth() + 1));
    return nextPaymentDate.toISOString();
  };

export default mongoose.model("PartnerSubscription",PartnerSubscriptionSchema)