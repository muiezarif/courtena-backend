import mongoose from "mongoose";

const { Schema } = mongoose

const PartnerSchema = new mongoose.Schema(
    {
        first_name: {
            type: String,
        },
        last_name: {
            type: String,
        },
        username: {
            type: String,
            default:""
            // required: true,
            // unique: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },
        country: {
            type: String
        },
        img: {
            type: String
        },
        city: {
            type: String
        },
        phone: {
            type: String
        },
        password: {
            type: String,
            required: true
        },
        isSubscribed: {
            type: Boolean,
            default: false
        },
        verified:{
            type:Boolean,
            default:false
        },
        fp: {
            type: String,
            default: ""
        },
        vt:{
            type: String,
            default: ""
        }
    },
    {
        timestamps: true
    });

export default mongoose.model("Partner", PartnerSchema)