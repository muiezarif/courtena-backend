import mongoose from "mongoose";

const { Schema } = mongoose;

const CourtReview = new mongoose.Schema({
    courtId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Court',
        required: true,
    },
    editedFields: {
        // Store the fields that were edited
        type: Object,
        required: true,
    },
    reviewed: {
        type: Boolean,
        default: false,
    },
},
{
    timestamps: true,
});

export default mongoose.model("CourtReview", CourtReview);