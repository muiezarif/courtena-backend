import mongoose from "mongoose";

const {Schema} = mongoose

const FavoriteVenueSchema = new mongoose.Schema(
{
    fav:{
        type:Boolean,
        required:true
    },
    venue:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Venue'
    },
    customer:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'Customer'
    },

},
{
    timestamps:true
});

export default mongoose.model("FavoriteVenue",FavoriteVenueSchema)