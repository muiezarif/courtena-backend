import mongoose from "mongoose";

const { Schema } = mongoose;

const VenueReview = new mongoose.Schema({
    venueId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Venue',
        required: true,
    },
    editedFields: {
        // Store the fields that were edited
        type: Object,
        required: true,
    },
    reviewed: {
        type: Boolean,
        default: false,
    },
},
{
    timestamps: true,
});

export default mongoose.model("VenueReview", VenueReview);