import express from "express";
import { verifyPartner } from "../utils/verifyToken.js";
import { createCourt, deleteCourt, getAllCourts, getAllCourtsByPartner, getCourt, updateCourt } from "../controllers/court.js";
import multer from "multer"
import multerS3Transform from "multer-s3-transform";
import AWS from "aws-sdk";
import path from "path"
import fs from "fs"
import dotenv from "dotenv"
dotenv.config();
const router = express.Router();

// Configure AWS SDK
const spacesEndpoint = new AWS.Endpoint(process.env.SPACES_END_POINT);
const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.SPACES_ACCESS_KEY,
  secretAccessKey: process.env.SPACES_SECRET_ACCESS,
  region: process.env.SPACES_REGION,
});
// const storage = multer.diskStorage({
//     destination:(req,file,cb) => {
//         console.log(req)
//         const venueId = req.body.venue
//         const courtName = req.body.title
//         const textWithoutSpaces = courtName.replace(/[ \t\n@#*]/g, "");
//         const destinationPath = `./images/${venueId}/court/${textWithoutSpaces}`
//         fs.mkdirSync(destinationPath, { recursive: true });
//         cb(null,destinationPath)
//     },
//     filename:(req,file,cb) => {
//         // console.log("gg")
//         console.log(file)
//         cb(null,"court_img"+ path.extname(file.originalname))
//     }
// })


const upload = multer({
    storage:multerS3Transform({
        s3: s3,
    bucket: process.env.SPACE_NAME,
    acl: "public-read",
    shouldTransform: false,
    key: async(req, file, cb) => {
        // Delete existing image if it exists
      console.log(req)
      const venueId = req.params.venueId;
      const partnerId = req.partner.id;
      const courtName = req.body.title;
      const textWithoutSpaces = courtName.replace(/[ \t\n@#*]/g, "");
      const folderPath = `images/${partnerId}/court/${textWithoutSpaces}/`;
      await deleteExistingImagesInFolder(folderPath)
      const key = `images/${partnerId}/court/${textWithoutSpaces}/court_img${Date.now()}-${file.originalname}`;
      cb(null, key);
    },
    }),
    limits: {
      fileSize: 5 * 1024 * 1024, // 5 MB in bytes
    },})


    const deleteExistingImagesInFolder = async (folderPath) => {
        // Delete all existing images in the specified folder
        try {
            const existingObjects = await s3.listObjectsV2({ Bucket: process.env.SPACE_NAME, Prefix: folderPath }).promise();
    
            if (existingObjects.Contents.length > 0) {
                const objectsToDelete = existingObjects.Contents.map(obj => ({ Key: obj.Key }));
                await s3.deleteObjects({ Bucket: process.env.SPACE_NAME, Delete: { Objects: objectsToDelete } }).promise();
                console.log(`Deleted existing images in folder: ${folderPath}`);
            }
        } catch (error) {
            console.error(`Error deleting existing images: ${error.message}`);
        }
    };

//CREATE 
router.post("/create/:venueId",verifyPartner,upload.single("image"), createCourt)
//UPDATE
router.put("/:id/update",verifyPartner,upload.single("image"), updateCourt)
//DELETE
router.delete("/delete/:id/:venueId",verifyPartner, deleteCourt)
//GET ALL VENUES BY PARTNER
router.get("/:partnerid",verifyPartner, getAllCourtsByPartner)
//GET
router.get("/get-court-detail/:id", getCourt)
//GET ALL
router.get("/", getAllCourts)

export default router