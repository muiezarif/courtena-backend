import express from "express";
import { verifyCustomer } from "../utils/verifyToken.js";
import { addCustomerCard, customerCreateBooking, deleteSavedCard, getCustomer, getCustomerBookingDetail, getCustomerBookings, getCustomerCourtInfo, getCustomerCourts, getCustomerCourtsBySearch, getCustomerHomeData, getCustomerPartnerCourtsByVenue, getCustomerVenueDetail, getCustomerVenues, getCustomerVenuesBySearch, getSavedCards, getVerifiedCard, toggleFavVenue, updateCustomer,cancelVoidCustomerReservation, getCustomerIsFavVenue, getCustomerFavVenues, cancelRefundCustomerReservation, checkRegisteredPhone, checkLoginPhone, getSettlementsData, searchVenuesByName, getApplePayInfo } from "../controllers/customer.js";

const router = express.Router();

router.get("/get-customer-info/:id",verifyCustomer,getCustomer)
router.get("/get-home-data",verifyCustomer,getCustomerHomeData)
router.get("/get-venues",verifyCustomer,getCustomerVenues)
router.get("/get-venue-detail/:venueid",verifyCustomer,getCustomerVenueDetail)
router.get("/get-courts",verifyCustomer,getCustomerCourts)
router.post("/get-venues-by-search/",verifyCustomer,getCustomerVenuesBySearch)
router.post("/get-venues-by-name/",verifyCustomer,searchVenuesByName)
router.get("/get-courts/:searchterm",verifyCustomer,getCustomerCourtsBySearch)
router.get("/get-partner-venue-courts/:partnerid/:venueid",verifyCustomer,getCustomerPartnerCourtsByVenue)
router.get("/get-customer-bookings/:customerid",verifyCustomer,getCustomerBookings)
router.get("/get-court-info/:courtid",verifyCustomer,getCustomerCourtInfo)
router.get("/get-booking-detail/:bookingid/:customerid",verifyCustomer,getCustomerBookingDetail)
router.put("/toggle-favorite-venue/", verifyCustomer,toggleFavVenue)

router.post("/customer-create-booking",verifyCustomer,customerCreateBooking)
// router.post("/customer-create-booking-pdf",generatepdf)
router.post("/toggle-fav-venue",verifyCustomer,toggleFavVenue)
router.get("/get-customer-isfav-venue/:customerId/:venueId",verifyCustomer,getCustomerIsFavVenue)
router.get("/get-customer-fav-venues/:customerId",verifyCustomer,getCustomerFavVenues)

router.put("/customer-update-info/:id",verifyCustomer,updateCustomer)
router.post("/save-customer-card",verifyCustomer,addCustomerCard)
router.delete("/delete-customer-card/:token/:customerid",verifyCustomer,deleteSavedCard)
router.get("/get-saved-cards/:customerid",verifyCustomer,getSavedCards)
router.get("/thanks-verify-card/",getVerifiedCard)
router.post("/customer-cancel-reservation/",verifyCustomer,cancelVoidCustomerReservation)
router.post("/customer-refund-reservation/",verifyCustomer,cancelRefundCustomerReservation)
router.post("/check-registered-phone/",checkRegisteredPhone)
router.post("/check-login-phone/",checkLoginPhone)
router.post("/get-settlements-data/",getSettlementsData)
router.post("/get-apple-pay-info/",getApplePayInfo)



export default router