import express from "express";
import { verifyAdmin } from "../utils/verifyToken.js";
import { approveCourt, approveEditCourtReview, approveEditVenueReview, approveFirstTimeCourtReview, approveFirstTimeVenueReview, approveVenue, disapproveCourt, disapproveVenue, getAdminBookings, getAdminCourtDetails, getAdminCourtInReview, getAdminCourtInReviewDetails, getAdminCustomers, getAdminEditCourtInReviewDetails, getAdminEditVenueInReviewDetails, getAdminReviewsCount, getAdminVenueDetails, getAdminVenueInReview, getAdminVenueInReviewDetails, getAllSettlements, getPartnerBillingInfo, getPartnerBookings, getPartnerCommission, rejectEditCourtReview, rejectEditVenueReview, rejectFirstTimeCourtReview, rejectFirstTimeVenueReview, settlePaymentToPartner, updatePartnerCommission } from "../controllers/admin.js";


const router = express.Router();

//GET
router.get("/get-bookings",verifyAdmin,getAdminBookings)
router.get("/get-partner-commission/:partnerid",verifyAdmin,getPartnerCommission)
router.post("/update-partner-commission/:partnerid",verifyAdmin,updatePartnerCommission)
router.get("/get-reviews-counts",verifyAdmin,getAdminReviewsCount)
router.get("/get-in-review-courts",verifyAdmin,getAdminCourtInReview)
router.get("/get-in-review-venues",verifyAdmin,getAdminVenueInReview)
router.get("/get-in-review-venue-details/:venueid",verifyAdmin,getAdminVenueInReviewDetails)
router.get("/get-venue-details/:venueid",verifyAdmin,getAdminVenueDetails)
router.get("/get-in-review-court-details/:courtid",verifyAdmin,getAdminCourtInReviewDetails)
router.get("/get-court-details/:courtid",verifyAdmin,getAdminCourtDetails)
router.get("/get-edit-in-review-venue-details/:venueid",verifyAdmin,getAdminEditVenueInReviewDetails)
router.get("/get-edit-in-review-court-details/:courtid",verifyAdmin,getAdminEditCourtInReviewDetails)
router.post("/approve-first-time-venue-review",verifyAdmin,approveFirstTimeVenueReview)
router.post("/approve-venue",verifyAdmin,approveVenue)
router.post("/approve-court",verifyAdmin,approveCourt)
router.post("/disapprove-venue",verifyAdmin,disapproveVenue)
router.post("/disapprove-court",verifyAdmin,disapproveCourt)
router.post("/reject-first-time-venue-review",verifyAdmin,rejectFirstTimeVenueReview)
router.post("/approve-first-time-court-review",verifyAdmin,approveFirstTimeCourtReview)
router.post("/reject-first-time-court-review",verifyAdmin,rejectFirstTimeCourtReview)
router.post("/approve-edit-venue-review",verifyAdmin,approveEditVenueReview)
router.post("/reject-edit-venue-review",verifyAdmin,rejectEditVenueReview)
router.post("/approve-edit-court-review",verifyAdmin,approveEditCourtReview)
router.post("/reject-edit-court-review",verifyAdmin,rejectEditCourtReview)
router.get("/get-customers",verifyAdmin,getAdminCustomers)
router.get("/get-partner-bookings/:partnerid",verifyAdmin,getPartnerBookings)
router.get("/get-admin-settlements/:adminid",verifyAdmin,getAllSettlements)
router.get("/settle-payment/:settlementid",verifyAdmin,settlePaymentToPartner)
router.get("/get-partner-billing-info/:partnerid",verifyAdmin,getPartnerBillingInfo)

export default router