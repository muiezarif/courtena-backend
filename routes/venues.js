import express from "express";
import Venue from "../models/Venue.js";
import { createError } from "../utils/error.js"
import { createVenue, deleteVenue, getAllVenues, getAllVenuesByPartner, getVenue, updateVenue } from "../controllers/venue.js";
import { verifyPartner } from "../utils/verifyToken.js";
import multer from "multer"
import multerS3 from "multer-s3";
import multerS3Transform from "multer-s3-transform";

import AWS from "aws-sdk";
import path from "path"
import fs from "fs"
import dotenv from "dotenv"
dotenv.config();
const router = express.Router();


const spacesEndpoint = new AWS.Endpoint(process.env.SPACES_END_POINT);

const s3 = new AWS.S3({
  endpoint: spacesEndpoint,
  accessKeyId: process.env.SPACES_ACCESS_KEY,
  secretAccessKey: process.env.SPACES_SECRET_ACCESS,
  region: process.env.SPACES_REGION,
});

const upload = multer({
  storage: multerS3Transform({
    s3: new AWS.S3({
      endpoint: spacesEndpoint,
      accessKeyId: process.env.SPACES_ACCESS_KEY,
      secretAccessKey: process.env.SPACES_SECRET_ACCESS,
      region: process.env.SPACES_REGION,
    }),
    bucket: process.env.SPACE_NAME, // Set the bucket parameter
    acl: "public-read", // Set the access control list to public-read for the uploaded files
    shouldTransform: false,
    key: async (req, file, cb) => {

      const partnerId = req.partner.id;
      const venueName = req.body.name.replace(/[ \t\n@#*]/g, "");
      const key = `images/${partnerId}/venue/${venueName}/${Date.now()}-${file.originalname}`;
      cb(null, key);
    },
    
  }),
  limits: {
    fileSize: 5 * 1024 * 1024, // 5 MB in bytes
  },
});

async function deleteExistingImages(req) {
  const venue = await Venue.findById(req.params.id)
  const partnerId = req.partner.id;
  const venueName = venue.name.replace(/[ \t\n@#*]/g, "");
  const folderPath = `images/${partnerId}/venue/${venueName}/`;

  const existingObjects = await s3.listObjectsV2({ Bucket: process.env.SPACE_NAME, Prefix: folderPath }).promise();

  if (existingObjects.Contents.length > 0) {
    await deleteObjectsSequentially(existingObjects.Contents);
  }
}

async function deleteObjectsSequentially(objects) {
  if (objects.length === 0) {
    return; // Exit condition
  }

  const obj = objects.shift(); // Remove the first object from the array

  await s3.deleteObject({ Bucket: process.env.SPACE_NAME, Key: obj.Key }).promise();

  // Recursively delete the next object
  await deleteObjectsSequentially(objects);
}

// Define the middleware for deleting existing images
const deleteImagesMiddleware = async (req, res, next) => {
  try {
    // Perform deletion logic
    await deleteExistingImages(req);
    // Continue to the next middleware or route handler
    next();
  } catch (error) {
    console.error("Error deleting existing images:", error);
    res.status(500).json({ success: false, message: "Error deleting existing images" });
  }
};


// Configure AWS SDK


const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const partnerId = req.partner.id;
    const venueName = req.body.name;
    const textWithoutSpaces = venueName.replace(/[ \t\n@#*]/g, "");
    const destinationPath = `./images/${partnerId}/venue/${textWithoutSpaces}`;
    // req.check = true
    fs.mkdirSync(destinationPath, { recursive: true });
    // Check if the folder already exists
    if (req.body.updateType == "complete") {
      if (fs.existsSync(destinationPath)) {
        fs.readdirSync(destinationPath).forEach((file) => {
          const filePath = path.join(destinationPath, file);
          console.log("Removing file: " + filePath);
          fs.unlinkSync(filePath);
        });
      } else {
        console.log("Destination folder does not exist. No files to remove.");
      }
    }


    cb(null, destinationPath);
  },
  filename: (req, file, cb) => {
    req.body.updateType = "false"
    cb(null, Date.now() + path.extname(file.originalname));
  }
});

// const upload = multer({
//     storage: storage, limits: {
//         fileSize: 5 * 1024 * 1024, // 5 MB in bytes
//     },
// });


// const upload = multer({dest:"./images"})



//CREATE 
router.post("/create", verifyPartner, upload.array('photos', 5), createVenue)
//UPDATE
router.put("/:id/update", verifyPartner, deleteImagesMiddleware,upload.array('photos', 5), updateVenue)
//DELETE
router.delete("/:id/delete", verifyPartner, deleteVenue)
//GET ALL VENUES BY PARTNER
router.get("/:partnerid", verifyPartner, getAllVenuesByPartner)
//GET
router.get("/get-venue/:id", getVenue)

//GET ALL
router.get("/", getAllVenues)


export default router