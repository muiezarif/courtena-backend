import express from "express";
import { verifyPartner } from "../utils/verifyToken.js";
import { getPartnerCustomers,getPartnerBookings, getPartnerDashboardData, getPartnerSettlements, manualPartnerBooking, manualPartnerBlockBooking, updateSchema, manualPartnerDeleteBooking, getPartner } from "../controllers/partner.js";

const router = express.Router();

//GET
router.get("/customers/:partnerid",verifyPartner,getPartnerCustomers)
router.get("/get-partner-bookings/:partnerid",verifyPartner,getPartnerBookings)
router.post("/create-partner-manual-booking/",verifyPartner,manualPartnerBooking)
router.post("/delete-partner-manual-booking/:bookingid",verifyPartner,manualPartnerDeleteBooking)
router.post("/create-partner-manual-block-booking/",verifyPartner,manualPartnerBlockBooking)
router.post("/delete-partner-manual-block-booking/:bookingid",verifyPartner,manualPartnerDeleteBooking)
router.get("/get-partner-settlements/:partnerid",verifyPartner,getPartnerSettlements)
router.get("/get-partner-dashboard-data/:partnerid",verifyPartner,getPartnerDashboardData)
router.get("/updateSchema",updateSchema)
router.get("/get-partner-info/:id",getPartner)
export default router