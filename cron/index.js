import cron from "node-cron"
import PartnerSubscription from "../models/PartnerSubscription.js";
import Partner from "../models/Partner.js";

export const cronJobToCheckPartnerSubscriptions = async () => {
    // Run the cron job logic here (for demonstration, running it once immediately)
        // Schedule a cron job to run at the start of every month
        cron.schedule("0 0 15 * *", async () => {
            // Run the cron job logic here
             await runCronJobToUpdatePartner();
          }, {
            scheduled: true,
            timezone: "UTC", // Specify your timezone, e.g., "UTC" or "America/New_York"
          });
    
}

const runCronJobToUpdatePartner = async () => {
    try {
        // Get all partner subscriptions
        const partnerSubscriptions = await PartnerSubscription.find();

        // Get the current date
        const currentDate = new Date();

        // Iterate through each partner subscription
        for (const subscription of partnerSubscriptions) {
            // Calculate the next payment date
            const nextPaymentDate = new Date(subscription.nextPaymentDate);

            // Check if the nextPaymentDate is in the past or equal to the current date
            const isPaymentDue = nextPaymentDate <= currentDate;

            // Update the corresponding Partner model
            await Partner.updateOne(
                { _id: subscription.partner },
                { $set: { isSubscribed: !isPaymentDue, nextPaymentDate: nextPaymentDate.toISOString() } }
            );

            console.log(`Updated Partner with ID ${subscription.partner} for subscription ID ${subscription._id}. isSubscribed set to ${!isPaymentDue}`);
        }
    } catch (error) {
        console.error("Error updating partners:", error);
    }
};